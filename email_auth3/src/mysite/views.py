# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render,redirect
from django.core.validators import validate_email
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.contrib import messages
from django.contrib.auth import login as LOGIN
from django.contrib.auth import logout as LOGOUT
from .models import Account
# Create your views here.
def home(request):
    return render(request, "home.html")

def index(request):
    if request.user.is_authenticated():
        return render(request,"index.html")
    return render(request,"home.html")

def create(request):
    error = []
    username = request.POST.get("username")
    email=request.POST.get("email")
    password=request.POST.get("password")

    try:
        validate_email(email)
    except ValidationError:
        error = error + ["Please enter a valid email."]



    email_exists = Account.objects.filter(email=email).count()
    if email_exists:
        raise ValidationError(u'Accout with given email already exists')
    error = error + (["Account with given email already exists."] if email_exists > 0 else [])
        # return render(request,'register_fail.html')

    username_exists = Account.objects.filter(username=username).count()
    if username_exists:
        raise ValueError(u'Account with given username already exists')
    error = error + (["Account with given username already exists."] if username_exists > 0 else [])

    if error != []:
        messages.add_message(request, messages.ERROR, "<br>".join(error))
        # return redirect(home)
        # return redirect(index)
        # return render(request,'register_fail.html')

    account = Account(
        username=username,
        email=email,
    )
    account.set_password(password)
    account.save()
    print "Done"
    return render(request,'register_success.html')

def login(request):
    # pass
    username = request.POST.get('username')
    password = request.POST.get('password')
    try:
        user = Account.objects.get(username=username)
    except ObjectDoesNotExist:
        messages.add_message(request, messages.ERROR, "Invalid username or password.")
        return render(request, 'login_fail.html')
    is_authenticated = user.check_password(password)

    if is_authenticated:
        LOGIN(request,user)
        return render(request,'index.html')
    else:
        return render(request,'login_fail.html')
def logout(request):
    LOGOUT(request)
    return render(request,'logout.html')
