# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import UserManager
# Create your models here.

class Account(AbstractBaseUser):
    username=models.CharField(max_length=127,unique=True)
    email=models.CharField(max_length=255,unique=True)
    # password = models.CharField(max_length=255) # Already covered in AbstractBaseUser

    objects = UserManager()
    USERNAME_FIELD = "username"

    def get_full_name(self):
        return self.name

    def get_short_name(self):
        return self.name

    def get_profile(self):
        return self.profile

    def __str__(self):
        return self.username
