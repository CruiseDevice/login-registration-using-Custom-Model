from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$',views.home,name='mysite_home'),
    url(r'^create/',views.create,name='mysite_create'),
    url(r'^login/',views.login,name='mysite_login'),
    url(r'^logout/',views.logout,name='mysite_logout')
    # url(r'^login/login_fail',views.login,name="mysite_login_fail")
]
